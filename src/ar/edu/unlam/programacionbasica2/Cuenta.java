package ar.edu.unlam.programacionbasica2;

/**
 * Created by maty on 10/4/17.
 */
public class Cuenta {

  private String nombreTitular;
  private Double saldo;
  private Integer cantidadExtracciones;

  /*
   ES PUBLIC PORQUE AL SER CONSTANTE NO PUEDE CAMBIAR SU VALOR
   ENTONCES EN ESTE CASO NO SE ROMPE EL ENCAPSULAMIENTO
    */
  public final Integer arancelPorCada5Extracciones = 1;

  /*
    AGREGAMOS UN CONSUTRUCTOR VACIO PARA PODER REALIZAR TESTS SIN TENER QUE
    CREAR UNA CUENTA CON TODOS LOS ATRIBUTOS
   */
  public Cuenta(){

    this.saldo = 0d;
    this.inicialiarCantidadExtracciones();
  }


  public Cuenta(String nombreTitular, Double saldo) {

    this.nombreTitular = nombreTitular;
    this.saldo = saldo;

    this.inicialiarCantidadExtracciones();
  }


  private void inicialiarCantidadExtracciones(){

    //AL CREAR UN CUENTA NO TIENE SENTIDO PASAR COMO PARAMETRO LA CANTIDAD DE EXTRACCIONES
    this.cantidadExtracciones = 0;
  }

  public String getNombreTitular() {
    return nombreTitular;
  }

  public Double getSaldo() {
    return saldo;
  }

  public Integer getCantidadExtracciones() {
    return cantidadExtracciones;
  }

  public void depositar(Double saldoADepositar) {

    //EN POSTERIORES CLASES VAMOS APRENDER A TRATAR ESTOS CASOS
    if(saldo >=0 )
      this.saldo += saldoADepositar;
  }

  public void extraer(Double saldoAExtraer) {


    //EN POSTERIORES CLASES VAMOS APRENDER A TRATAR ESTOS CASOS

    if( tieneSaldo(saldoAExtraer) ){
      this.saldo -= saldoAExtraer;
    }

    if( laCantidadDeExtraccionesEsMultiploDe5() ){
      this.saldo -=arancelPorCada5Extracciones;
    }

    this.cantidadExtracciones++;
  }

  private Boolean tieneSaldo(Double saldoAExtraer){

      double saldoParcial = saldo - saldoAExtraer;

      if( laCantidadDeExtraccionesEsMultiploDe5() ){
        saldoParcial -= arancelPorCada5Extracciones;
      }


      return  saldoParcial>= 0;
  }

  private Boolean laCantidadDeExtraccionesEsMultiploDe5(){

    return (this.cantidadExtracciones +1) % 5 == 0;
  }


}
