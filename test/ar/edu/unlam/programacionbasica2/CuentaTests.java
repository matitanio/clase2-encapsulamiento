package ar.edu.unlam.programacionbasica2;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Created by maty on 10/4/17.
 */
public class CuentaTests {

  @Test
  public void queSePuedaCrearUnaCuenta(){

    Cuenta unaCuenta = new Cuenta();
    assertNotNull(unaCuenta);

  }


  @Test
  public void queSePuedaConsultarElSaldo(){


    Cuenta unaCuenta = new Cuenta();

    Double saldoEsperado;
    assertEquals(0, unaCuenta.getSaldo(), 0.01);
  }


  @Test
  public void queSePuedaDepositar(){

    Double saldoInicial = 100d;
    Cuenta unaCuenta = new Cuenta("Juan Perez",  saldoInicial);

    Double saldoADepositar = 80d;

    unaCuenta.depositar(saldoADepositar);

    assertEquals(saldoInicial + saldoADepositar, unaCuenta.getSaldo(), 0.01);

  }

  @Test
  public void queSePuedaExtraer(){

    Double saldoInicial = 100d;
    Cuenta unaCuenta = new Cuenta("Juan Perez",  saldoInicial);

    Double saldoAExtraer = 30d;


    unaCuenta.extraer(saldoAExtraer);

    assertEquals(saldoInicial - saldoAExtraer, unaCuenta.getSaldo(), 0.01);

  }

  @Test
  public void queSePuedaExtraerYLuegoDe5ExtraccionesMeCobre(){

    Double saldoInicial = 100d;
    Cuenta unaCuenta = new Cuenta("Juan Perez",  saldoInicial);

    Double saldoAExtraer = 10d;
    unaCuenta.extraer(saldoAExtraer);
    unaCuenta.extraer(saldoAExtraer);
    unaCuenta.extraer(saldoAExtraer);
    unaCuenta.extraer(saldoAExtraer);
    unaCuenta.extraer(saldoAExtraer);

    //MAS ADELANTE VAMOS A VER COMO HACER ESTO DE OTRA FORMA
    Double saldoTotalDebitado = saldoAExtraer * 5 + unaCuenta.arancelPorCada5Extracciones;


    assertEquals(saldoInicial - saldoTotalDebitado, unaCuenta.getSaldo(), 0.01);

  }
}
